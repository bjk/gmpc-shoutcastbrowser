/* gmpc-shoutcastbrowser (GMPC plugin)
 * Copyright (C) 2014-15 Ben Kibbey <bjk@luxsci.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <regex.h>
#include <errno.h>
#include <limits.h>
#include <glib/gi18n-lib.h>
#include <glib-2.0/glib.h>
#include <gmpc_easy_download.h>
#include <plugin.h>
#include <libmpd/libmpd.h>
#include <libmpd/libmpd-internal.h>
#include "shoutcast.h"

/*
 * Field 1: genre name
 * Field 2: genre id
 * Field 3: is sub-genre==1
 */
#define GENRE_RE "loadStationsByGenre\\('([^']*)', ([0-9]*), ([0-9]*)\\)"

/* This is really a database formatted file which is retrieved via HTTP POST.
 * The fields are separated by commas:
 *
 *     SID,NAME,ENCODING,BITRATE,GENRE,SONG,LISTENERS,...
 */
#define STATION_RE "\\{\"ID\":([0-9]*),\"Name\":\"([^\"]*)\",\"Format\":\"([^\"]*)\",\"Bitrate\":([0-9]*),\"Genre\":\"([^\"]*)\",\"CurrentTrack\":\"([^\"]*)\",\"Listeners\":([0-9]*),[^}]*\\}"

static struct genre_s **genres;
static regex_t genre_re, station_re;
static int ngenres;
static struct
{
  const char *markup;
  const char c;
} markup_entities[] = {
  {"&quot;", '"'},
  {"\\u0022", '"'},
  {"&apos;", '\''},
  {"\\u0027", '\''},
  {"&amp;", '&'},
  {"\\u0026", '&'},
  {"&lt;", '<'},
  {"\\u003C", '<'},
  {"\\u003c", '<'},
  {"&gt;", '>'},
  {"\\u003E", '>'},
  {"\\u003e", '>'},
  {NULL, 0}
};

gchar *shoutcastbrowser_cachedir;

static char *
unescape_entities (char *str)
{
  int i;

  for (i = 0; markup_entities[i].markup;)
    {
      size_t len = strlen (str);
      char *p = g_strstr_len (str, len, markup_entities[i].markup);

      if (p)
	{
	  size_t n = strlen (p);
	  gchar *t = str + (len - n);

	  *t++ = markup_entities[i].c;
	  p += strlen (markup_entities[i].markup);

	  while (*p)
	    {
	      *t++ = *p++;
	    }

	  *t = 0;
	  i = 0;
	  continue;
	}

      i++;
    }

  return str;
}

static void
free_stations (struct genre_s *genre)
{
  struct station_s **s;

  if (!genre)
    return;

  for (s = genre->stations; s && *s; s++)
    {
      free ((*s)->name);
      free ((*s)->id);
      free ((*s)->genre);
      free ((*s)->type);
      free ((*s)->track);
      free (*s);
    }

  free (genre->stations);
  genre->stations = NULL;
  genre->s_total = 0;
}

static void
free_genres (struct genre_s **g)
{
  struct genre_s **p;

  for (p = g; p && *p; p++)
    {
      struct genre_s *tmp = *p;

      free_stations (tmp);
      free (tmp->name);
      free_genres (tmp->sub);
      free (tmp);
    }

  free (g);
}

static int
extract_from_re (regmatch_t m, const char *str, char *dst, size_t size)
{
  char *tmp = dst;
  size_t len = 0;

  if (m.rm_so != -1)
    {
      const char *p = str + m.rm_so;
      int i;

      for (i = m.rm_so; i < m.rm_eo && len < size - 1; i++, len++)
	*tmp++ = *p++;

      *tmp = 0;
      return m.rm_eo;
    }

  return 0;
}

static struct genre_s **
add_genre (struct genre_s **dst, size_t *size,
	   const char *genre, struct genre_s **res)
{
  size_t n = *size;

  dst = realloc (dst, (n + 2) * sizeof (struct genre_s **));
  dst[n] = calloc (1, sizeof (struct genre_s));
  dst[n]->name = strdup (unescape_entities ((char *) genre));

  if (res)
    *res = dst[n];

  n++;
  dst[n] = NULL;
  *size = n;
  return dst;
}

static void
save_cache (const char *data, size_t len, struct genre_s *genre)
{
  char *e = gmpc_easy_download_uri_escape (genre ? genre->name : "genres");
  char *path = g_strdup_printf ("%s/%s.html", shoutcastbrowser_cachedir, e);
  GIOChannel *io = g_io_channel_new_file (path, "w+", NULL);
  size_t wrote;

  g_free (e);
  g_free (path);
  if (io)
    {
      g_io_channel_write_chars (io, data, len, &wrote, NULL);
      g_io_channel_shutdown (io, TRUE, NULL);
      g_io_channel_unref (io);
    }
}

static int
parse_doc (const char *data, size_t len, struct genre_s *for_genre)
{
  int n = 3;
  regmatch_t pmatch[9];
  const char *p;
  int x = 0;
  char g[255], id[255], is_subgenre[2];
  struct genre_s **newp = NULL, *cur = NULL;
  size_t total = 0;
  struct station_s **stations = NULL;
  size_t s_total = 0;

  if (!for_genre)
    {
      newp = add_genre (newp, &total, N_((const char *) "Search"), NULL);
    }

  for (p = data; p && *p;)
    {
      if (for_genre)
	{
	  struct station_s *station;
	  char str[16];
	  char s_genre[128];
	  char track[64];

	  n = 8;
	  if (regexec (&station_re, p, n, pmatch, 0) == REG_NOMATCH)
	    break;

	  stations = realloc (stations,
			      (s_total + 2) * sizeof (struct station_s **));
	  stations[s_total] = calloc (1, sizeof (struct station_s));
	  station = stations[s_total];

	  x = extract_from_re (pmatch[1], p, id, sizeof (id));
	  x = extract_from_re (pmatch[2], p, g, sizeof (g));
	  x = extract_from_re (pmatch[3], p, str, sizeof (str));
	  station->type = strdup (str);
	  x = extract_from_re (pmatch[4], p, str, sizeof (str));
	  station->bitrate = atoi (str);
	  x = extract_from_re (pmatch[5], p, s_genre, sizeof (s_genre));
	  x = extract_from_re (pmatch[6], p, track, sizeof (track));
	  x = extract_from_re (pmatch[7], p, str, sizeof (str));
	  station->listeners = atoi (str);
	  p += x;
	  station->name = strdup (unescape_entities (g));
	  station->id = strdup (id);
	  station->genre = strdup (unescape_entities (s_genre));
	  station->track = strdup (unescape_entities (track));
	  s_total++;
	  stations[s_total] = NULL;
	  continue;
	}

      n = 4;
      if (regexec (&genre_re, p, n, pmatch, 0) != REG_NOMATCH)
	{
	  x = extract_from_re (pmatch[1], p, g, sizeof (g));
	  x = extract_from_re (pmatch[2], p, id, sizeof (id));
	  x =
	    extract_from_re (pmatch[3], p, is_subgenre, sizeof (is_subgenre));
	  p += x;

	  if (!atoi (is_subgenre))
	    newp = add_genre (newp, &total, g, &cur);
	  else
	    cur->sub = add_genre (cur->sub, &cur->g_total, g, NULL);
	}
      else
	break;
    }

  if (for_genre)
    {
      free_stations (for_genre);
      for_genre->stations = stations;
      for_genre->s_total = s_total;
      save_cache (data, len, for_genre);
      return 0;
    }

  if (total)
    {
      free_genres (genres);
      ngenres = total;
      genres = newp;
      save_cache (data, len, NULL);
      return 0;
    }

  free_genres (newp);
  return 1;
}

static int
load_cache (struct genre_s *genre)
{
  char *e = gmpc_easy_download_uri_escape (genre ? genre->name : "genres");
  char *path = g_strdup_printf ("%s/%s.html", shoutcastbrowser_cachedir, e);
  char *data;
  size_t len;

  g_free (e);
  if (g_file_get_contents (path, &data, &len, NULL) == FALSE)
    {
      g_free (path);
      return 1;
    }

  g_free (path);
  shoutcastbrowser_data_ready (data, len, genre);
  g_free (data);
  return 0;
}

int
shoutcastbrowser_init (int *refresh)
{
  int e;
  char buf[LINE_MAX];
  static int init;

  if (!init)
    {
      e = regcomp (&genre_re, GENRE_RE, REG_EXTENDED);
      if (e)
	{
	  regerror (e, &genre_re, buf, sizeof (buf));
	  fprintf (stderr, "%s\n", buf);
	  return 1;
	}

      e = regcomp (&station_re, STATION_RE, REG_EXTENDED);
      if (e)
	{
	  regerror (e, &station_re, buf, sizeof (buf));
	  fprintf (stderr, "%s\n", buf);
	  return 1;
	}
    }

  init = 1;
  *refresh = load_cache (NULL);
  return 0;
}

void
shoutcastbrowser_deinit ()
{
  regfree (&genre_re);
  regfree (&station_re);
  free_genres (genres);
  genres = NULL;
  ngenres = 0;
  free (shoutcastbrowser_cachedir);
}

void
shoutcastbrowser_data_ready (const char *data, size_t len,
			     struct genre_s *genre)
{
  parse_doc (data, len, genre);
}

static void
free_mpd_userdata (void *data)
{
  free (data);
}

MpdData *
shoutcastbrowser_station_list (struct genre_s *genre)
{
  MpdData *list = NULL;
  int i;

  load_cache (genre);
  if (!genre->s_total)
    return NULL;

  for (i = 0; i < genre->s_total; i++)
    {
      mpd_Song *song = mpd_newSong ();

      song->title = g_strdup (genre->stations[i]->name);
      song->genre = g_strdup (genre->stations[i]->genre);
      song->comment = g_strdup (genre->stations[i]->type);
      song->file = NULL;
      song->disc = g_strdup_printf ("%i", genre->stations[i]->listeners);
      song->date = g_strdup_printf ("%i", genre->stations[i]->bitrate);
      song->name = g_strdup (genre->stations[i]->track);
      list = mpd_new_data_struct_append (list);
      list->type = MPD_DATA_TYPE_SONG;
      list->song = song;
      list->userdata = strdup (genre->stations[i]->id);
      list->freefunc = free_mpd_userdata;
    }

  free_stations (genre);
  return list;
}

MpdData *
shoutcastbrowser_genre_list ()
{
  MpdData *list = NULL;
  int i;

  for (i = 0; i < ngenres; i++)
    {
      list = mpd_new_data_struct_append (list);
      list->type = MPD_DATA_TYPE_TAG;
      list->tag_type = MPD_TAG_ITEM_GENRE;
      list->tag = strdup (genres[i]->name);
      list->userdata = genres[i];
    }

  return list;
}

MpdData *
shoutcastbrowser_subgenre_list (struct genre_s *genre)
{
  MpdData *list = NULL;
  int i;

  for (i = 0; i < genre->g_total; i++)
    {
      list = mpd_new_data_struct_append (list);
      list->type = MPD_DATA_TYPE_TAG;
      list->tag_type = MPD_TAG_ITEM_GENRE;
      list->tag = strdup (genre->sub[i]->name);
      list->userdata = genre->sub[i];
    }

  return list;
}

struct genre_s *
shoutcastbrowser_genre_by_index (int index)
{
  if (index < 0 || index > ngenres)
    return NULL;

  return genres[index];
}
