/* gmpc-shoutcastbrowser (GMPC plugin)
 * Copyright (C) 2014-15 Ben Kibbey <bjk@luxsci.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <string.h>
#include <gdk/gdkkeysyms.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>
#include <glade/glade.h>
#include <plugin.h>
#include <gmpc_easy_download.h>
#include <metadata.h>
#include <gmpc-mpddata-model.h>
#include <gmpc-extras.h>
#include <playlist3-messages.h>
#include <libmpd/debug_printf.h>
#include "shoutcast.h"

enum
{
  SHOUTCAST_NONE,
  SHOUTCAST_GENRES,
  SHOUTCAST_SUB_GENRES,
  SHOUTCAST_STATIONS,
  SHOUTCAST_PLS,
  SHOUTCAST_SEARCH,
};

struct download_cb_data_s
{
  void *widget;
  void *data;
  int state;
};

static int station_column_width, genre_column_width;
static GtkWidget *shoutcast_tree;
static GtkWidget *shoutcast_pb = NULL, *shoutcast_cancel = NULL;
static GtkWidget *shoutcast_vbox = NULL;
static GmpcMpdDataModel *sb_store = NULL;
static GtkWidget *shoutcast_search_find, *shoutcast_search_entry;
static GtkWidget *treeviews[3] = { NULL, NULL, NULL };

static GtkTreeRowReference *shoutcastbrowser_ref = NULL;
static int downloading;
static GtkWidget *cache_cb;
static int play_after_fetch;
static gchar *search_text;
static struct genre_s *current_genre;

extern GladeXML *pl3_xml;
gmpcPlugin plugin;
int plugin_api_version = PLUGIN_API_VERSION;

static void
shoutcast_mpd_status_changed (MpdObj * mi, ChangedStatusType what, void *data)
{
}

static int
shoutcast_get_enabled ()
{
  return cfg_get_single_value_as_int_with_default (config, "shoutcastbrowser",
						   "enable", TRUE);
}

static void
shoutcast_cache_checkbox_changed (GtkToggleButton * b, gpointer data)
{
  gboolean val = gtk_toggle_button_get_active (b);

  cfg_set_single_value_as_int (config, "shoutcastbrowser",
			       "cache_stations", val);
}

gboolean
shoutcastbrowser_cache_stations ()
{
  return cfg_get_single_value_as_int_with_default (config,
						   "shoutcastbrowser",
						   "cache_stations", 1);
}

void
shoutcast_pref_construct (GtkWidget * container)
{
  GtkWidget *table;

  table = gtk_table_new (2, 2, FALSE);

  cache_cb = gtk_check_button_new_with_label (N_("Cache stations"));
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cache_cb),
				shoutcastbrowser_cache_stations ());
  g_signal_connect (G_OBJECT (cache_cb), "toggled",
		    G_CALLBACK (shoutcast_cache_checkbox_changed), NULL);
  gtk_table_attach (GTK_TABLE (table), cache_cb, 1, 2, 0, 1,
		    GTK_EXPAND | GTK_FILL, GTK_SHRINK | GTK_FILL, 0, 0);

  gtk_container_add (GTK_CONTAINER (container), table);
  gtk_widget_show_all (container);
}

void
shoutcast_pref_destroy (GtkWidget * container)
{
  GtkWidget *child = gtk_bin_get_child (GTK_BIN (container));
  if (child)
    gtk_widget_destroy (child);
}

static void
shoutcast_add (GtkWidget * cat_tree)
{
  GtkTreePath *path = NULL;
  GtkTreeIter iter;
  GtkListStore *pl3_tree =
    (GtkListStore *) gtk_tree_view_get_model (GTK_TREE_VIEW (cat_tree));
  gint pos =
    cfg_get_single_value_as_int_with_default (config, "shoutcastbrowser",
					      "position", 20);

  if (!cfg_get_single_value_as_int_with_default (config, "shoutcastbrowser",
						 "enable", TRUE))
    return;

  debug_printf (DEBUG_INFO, "Adding at position: %i", pos);
  playlist3_insert_browser (&iter, pos);
  gtk_list_store_set (GTK_LIST_STORE (pl3_tree), &iter,
		      PL3_CAT_TYPE, plugin.id,
		      PL3_CAT_TITLE, N_("SHOUTcast Browser"),
		      PL3_CAT_INT_ID, "",
		      PL3_CAT_ICON_ID, "shoutcastbrowser", -1);
  /**
   * Clean up old row reference if it exists
   */
  if (shoutcastbrowser_ref)
    {
      gtk_tree_row_reference_free (shoutcastbrowser_ref);
      shoutcastbrowser_ref = NULL;
    }
  /**
   * create row reference
   */
  path =
    gtk_tree_model_get_path (GTK_TREE_MODEL
			     (playlist3_get_category_tree_store ()), &iter);
  if (path)
    {
      shoutcastbrowser_ref =
	gtk_tree_row_reference_new (GTK_TREE_MODEL
				    (playlist3_get_category_tree_store ()),
				    path);
      gtk_tree_path_free (path);
    }
}

static void
shoutcast_set_enabled (int enabled)
{
  cfg_set_single_value_as_int (config, "shoutcastbrowser", "enable", enabled);
  if (enabled)
    {
      if (shoutcastbrowser_ref == NULL)
	{
	  shoutcast_add (GTK_WIDGET (playlist3_get_category_tree_view ()));
	}
    }
  else if (shoutcastbrowser_ref)
    {
      GtkTreePath *path =
	gtk_tree_row_reference_get_path (shoutcastbrowser_ref);
      if (path)
	{
	  GtkTreeIter iter;
	  if (gtk_tree_model_get_iter
	      (GTK_TREE_MODEL (playlist3_get_category_tree_store ()), &iter,
	       path))
	    {
	      gtk_list_store_remove (playlist3_get_category_tree_store (),
				     &iter);
	    }
	  gtk_tree_path_free (path);
	  gtk_tree_row_reference_free (shoutcastbrowser_ref);
	  shoutcastbrowser_ref = NULL;
	}
    }

  pl3_update_go_menu ();
}

#define TEST_CHAR(p,c) (*p && *++p == c)

static void
parse_pls (const char *data, size_t len)
{
  char **lines = g_strsplit (data, "\n", 0);
  char **p;
  char *last = NULL;
  int min = -1;

  for (p = lines; *p;)
    {
      gchar *tmp = NULL;

      if (!g_ascii_strncasecmp (*p, "File", 4) && g_ascii_isdigit (*(*p + 4)))
	{
	  tmp = *p + 4;

	  while (g_ascii_isdigit (*tmp))
	    tmp++;

	  if (*tmp++ != '=')
	    continue;

	  p++;
	  if (*p && !g_ascii_strncasecmp (*p, "Title", 5)
	      && g_ascii_isdigit (*(*p + 5)))
	    {
	      char *t = *p + 5;
	      int n;

	      if (!TEST_CHAR (t, '=') || !TEST_CHAR (t, '(')
		  || !TEST_CHAR (t, '#'))
		goto cont;

	      t++;
	      if (!g_ascii_isdigit (*t))
		goto cont;

	      if (!TEST_CHAR (t, ' ') || !TEST_CHAR (t, '-')
		  || !TEST_CHAR (t, ' '))
		goto cont;

	      t++;
	      if (!g_ascii_isdigit (*t))
		goto cont;

	      n = atoi (t);
	      if (min == -1 || n <= min)
		{
		  min = n;
		  last = tmp;
		}
	    }
	}

    cont:
      if (!last)
	last = tmp;

      p++;
    }

  if (last)
    mpd_playlist_queue_add (connection, last);

  mpd_playlist_queue_commit (connection);

  if (play_after_fetch)
    mpd_player_play (connection);

  play_after_fetch = 0;
}

static void
show_genre_list ()
{
  MpdData *sdata = shoutcastbrowser_genre_list ();

  if (sdata)
    {
      gmpc_mpddata_model_set_mpd_data (GMPC_MPDDATA_MODEL
				       (gtk_tree_view_get_model
					(GTK_TREE_VIEW (treeviews[0]))),
				       sdata);
      current_genre = shoutcastbrowser_genre_by_index (0);
    }

  gtk_widget_set_sensitive (treeviews[0], TRUE);
  gtk_widget_set_sensitive (treeviews[1], TRUE);
}

static gboolean
show_station_list (struct genre_s *genre)
{
  MpdData *sdata = shoutcastbrowser_station_list (current_genre);

  gmpc_mpddata_model_set_mpd_data (GMPC_MPDDATA_MODEL (sb_store), sdata);
  return sdata != NULL;
}

static void
download_cb (const GEADAsyncHandler * handle,
	     const GEADStatus status, gpointer data)
{
  struct download_cb_data_s *cb_data = data;
  GtkWidget *pb = cb_data->widget;

  if (status == GEAD_DONE)
    {
      goffset length;
      const char *hdata = gmpc_easy_handler_get_data (handle, &length);

      downloading = 0;

      if (length <= 0 || hdata == NULL)
	{
	  playlist3_show_error_message (N_("SHOUTcast transfer failed."),
					ERROR_WARNING);
	  g_free (cb_data);
	  return;
	}
      else if (cb_data->state != SHOUTCAST_PLS)
	shoutcastbrowser_data_ready (hdata, length, cb_data->data);

      switch (cb_data->state)
	{
	case SHOUTCAST_GENRES:
	  show_genre_list ();
	  break;
	case SHOUTCAST_STATIONS:
	case SHOUTCAST_SEARCH:
	  show_station_list (cb_data->data);
	  break;
	case SHOUTCAST_PLS:
	  parse_pls (hdata, length);
	  break;
	default:
	  break;
	}

      gtk_widget_hide (gtk_widget_get_parent (pb));
    }
  else if (status == GEAD_CANCELLED && pb)
    {
      gtk_widget_hide (gtk_widget_get_parent (pb));
      downloading = 0;
    }
  else if (status == GEAD_PROGRESS)
    {
      goffset length;
      goffset total = gmpc_easy_handler_get_content_size (handle);
      gmpc_easy_handler_get_data (handle, &length);
      if (pb && total > 0)
	{
	  double fraction = (100 * length) / (total);

	  gchar *size = g_format_size_for_display ((goffset) length);
	  gchar *strtotal = g_format_size_for_display ((goffset) total);
	  gchar *label =
	    g_strdup_printf (N_
			     ("Transferring from SHOUTcast (%s of %s done)"),
			     size, strtotal);
	  g_free (strtotal);
	  g_free (size);
	  gtk_progress_bar_set_text (GTK_PROGRESS_BAR (pb), label);
	  g_free (label);
	  gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (pb),
					 fraction / 100.0);
	}
      else
	{
	  if (pb)
	    gtk_progress_bar_pulse (GTK_PROGRESS_BAR (pb));
	}
    }

  if (status != GEAD_PROGRESS)
    g_free (cb_data);
}

static void
get_genre_list ()
{
  struct download_cb_data_s *cb_data =
    g_malloc0 (sizeof (struct download_cb_data_s));
  GEADAsyncHandler *h;

  downloading = 1;
  gmpc_mpddata_model_set_mpd_data (GMPC_MPDDATA_MODEL
				   (gtk_tree_view_get_model
				    (GTK_TREE_VIEW (treeviews[0]))), NULL);
  gmpc_mpddata_model_set_mpd_data (GMPC_MPDDATA_MODEL
				   (gtk_tree_view_get_model
				    (GTK_TREE_VIEW (treeviews[1]))), NULL);
  gmpc_mpddata_model_set_mpd_data (GMPC_MPDDATA_MODEL (sb_store), NULL);
  gtk_widget_show_all (gtk_widget_get_parent (shoutcast_pb));
  cb_data->widget = shoutcast_pb;
  cb_data->data = NULL;
  cb_data->state = SHOUTCAST_GENRES;
  h =
    gmpc_easy_async_downloader ("https://directory.shoutcast.com/",
				download_cb, cb_data);
  g_object_set_data (G_OBJECT (shoutcast_cancel), "handle", h);
  gtk_widget_set_sensitive (treeviews[0], FALSE);
  gtk_widget_set_sensitive (treeviews[1], FALSE);
}

static void
refresh_genre (struct genre_s *genre, gboolean search)
{
  struct download_cb_data_s *cb_data =
    g_malloc0 (sizeof (struct download_cb_data_s));
  char *tmp, *url;

  downloading = 1;
  cb_data->widget = shoutcast_pb;
  cb_data->data = genre;
  cb_data->state = search ? SHOUTCAST_SEARCH : SHOUTCAST_STATIONS;

  if (search)
    {
      gchar *e =
	gmpc_easy_download_uri_escape (gtk_entry_get_text
				       (GTK_ENTRY (shoutcast_search_entry)));

      url = g_strdup ("https://directory.shoutcast.com/Search/UpdateSearch");
      tmp = g_strdup_printf ("query=%s", e);
      //url = g_strdup("http://shoutcast.com/AdvancedSearch");
      //tmp = g_strdup_printf("genre=&station=&type=&artist=&song=");
    }
  else
    {
      tmp = g_strdup_printf ("genrename=%s", genre->name);
      url = g_strdup ("https://directory.shoutcast.com/Home/BrowseByGenre");
    }

  GEADAsyncHandler *handle = gmpc_easy_async_downloader2 (url, GEAD_POST, tmp,
							  "application/x-www-form-urlencoded; charset=UTF-8",
							  download_cb,
							  cb_data);
  fprintf (stderr, "fetching URL '%s'\n", tmp);
  g_free (url);
  g_free (tmp);
  g_object_set_data (G_OBJECT (shoutcast_cancel), "handle", handle);
}

static gboolean
is_search_genre (GtkTreeSelection * s)
{
  GtkTreeView *w = gtk_tree_selection_get_tree_view (s);
  GtkTreePath *path;

  if (w != GTK_TREE_VIEW (treeviews[0]))
    return FALSE;

  gtk_tree_view_get_cursor (GTK_TREE_VIEW (w), &path, NULL);

  if (path)
    {
      gint *indices = gtk_tree_path_get_indices (path);

      if (indices[0] == 0)
	return TRUE;
    }

  return FALSE;
}

static void
shoutcast_show_song_list_common (GtkTreeSelection * selection,
				 gpointer user_data)
{
  GtkTreeIter piter;
  GtkTreeView *tv = gtk_tree_selection_get_tree_view (selection);
  GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tv));
  struct genre_s *genrep = NULL;

  if (gtk_tree_selection_get_selected (selection, &model, &piter))
    {
      selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (tv));
      gtk_tree_model_get (model, &piter, MPDDATA_MODEL_USERDATA, &genrep, -1);
    }

  if (!genrep)
    return;

  if (show_station_list (genrep) && shoutcastbrowser_cache_stations ())
    return;

  gmpc_mpddata_model_set_mpd_data (GMPC_MPDDATA_MODEL (sb_store), NULL);
  refresh_genre (genrep, is_search_genre (selection));
}

static void
shoutcast_do_search (GtkWidget * button)
{
  struct genre_s *genre = g_object_get_data (G_OBJECT (button), "genre");

  if (genre)
    {
      refresh_genre (genre, TRUE);
    }
}

static void
shoutcast_search (struct genre_s *genre)
{
  g_object_set_data (G_OBJECT (shoutcast_search_find), "genre", genre);
  gtk_widget_show_all (gtk_widget_get_parent (shoutcast_search_find));
}

static void
shoutcast_show_genre_list (GtkTreeSelection * selection, gpointer user_data)
{
  gmpc_mpddata_model_set_mpd_data (GMPC_MPDDATA_MODEL (sb_store), NULL);

  if (downloading)
    return;

  GtkTreeIter piter;
  GtkTreeModel *model =
    gtk_tree_view_get_model (GTK_TREE_VIEW (treeviews[0]));
  struct genre_s *genre = NULL;

  if (gtk_tree_selection_get_selected (selection, &model, &piter))
    {
      selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeviews[1]));
      gtk_tree_model_get (model, &piter, MPDDATA_MODEL_USERDATA, &genre, -1);
    }

  if (!genre)
    return;

  GtkTreePath *path;
  gtk_tree_view_get_cursor (GTK_TREE_VIEW (treeviews[0]), &path, NULL);

  if (path)
    {
      gint *indices = gtk_tree_path_get_indices (path);

      if (indices[0] == 0)
	{
	  show_station_list (genre);
	  shoutcast_search (genre);
	  return;
	}
    }

  gtk_widget_hide (gtk_widget_get_parent (shoutcast_search_find));
  g_object_set_data (G_OBJECT (shoutcast_search_find), "genre", NULL);
  fprintf (stderr, "select SUBGENRE for '%s'\n", genre->name);
  MpdData *data = shoutcastbrowser_subgenre_list (genre);

  gmpc_mpddata_model_set_mpd_data (GMPC_MPDDATA_MODEL
				   (gtk_tree_view_get_model
				    (GTK_TREE_VIEW (treeviews[1]))), data);
  shoutcast_show_song_list_common (selection, NULL);
}

static void
shoutcast_show_song_list (GtkTreeSelection * selection, gpointer user_data)
{
  shoutcast_show_song_list_common (selection, user_data);
}

static void
shoutcast_download_cancel (GtkWidget * button)
{
  GEADAsyncHandler *handle = g_object_get_data (G_OBJECT (button), "handle");

  if (handle)
    {
      gmpc_easy_async_cancel (handle);
      g_object_set_data (G_OBJECT (button), "handle", NULL);
    }
}

static void
fetch_station (const gchar * id)
{
  struct download_cb_data_s *cb_data =
    g_malloc0 (sizeof (struct download_cb_data_s));
  GEADAsyncHandler *h;

  fprintf (stderr, "Fetching station %s\n", id);
  downloading = 1;
  gtk_widget_show_all (gtk_widget_get_parent (shoutcast_pb));
  cb_data->widget = shoutcast_pb;
  cb_data->state = SHOUTCAST_PLS;
  gchar *tmp =
    g_strdup_printf ("https://yp.shoutcast.com/sbin/tunein-station.pls?id=%s",
		     id);
  h = gmpc_easy_async_downloader (tmp, download_cb, cb_data);
  g_free (tmp);
  g_object_set_data (G_OBJECT (shoutcast_cancel), "handle", h);
}

static void
shoutcast_add_selected (GtkWidget * button, GtkTreeView * tree)
{
  GtkTreeModel *model = GTK_TREE_MODEL (sb_store);
  GtkTreeSelection *sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
  GList *list = gtk_tree_selection_get_selected_rows (sel, &model);
  if (list)
    {
      GList *node;

      for (node = list; node; node = g_list_next (node))
	{
	  GtkTreeIter iter;

	  if (gtk_tree_model_get_iter (model, &iter, node->data))
	    {
	      gchar *id;

	      gtk_tree_model_get (model, &iter, MPDDATA_MODEL_USERDATA,
				  &id, -1);
	      fetch_station (id);
	    }
	}

      g_list_foreach (list, (GFunc) gtk_tree_path_free, NULL);
      g_list_free (list);
    }
}

static void
shoutcast_replace_selected (GtkWidget * button, GtkTreeView * tree)
{
  mpd_playlist_clear (connection);
  shoutcast_add_selected (button, tree);
  play_after_fetch = 1;
}

static int
shoutcast_button_release_event (GtkWidget * tree,
				GdkEventButton * event, gpointer data)
{
  if (event->button == 1 && event->type == GDK_2BUTTON_PRESS)
    {
      shoutcast_replace_selected (NULL, (GtkTreeView *) tree);
      return TRUE;
    }
  else if (event->button != 3)
    return FALSE;

  GtkWidget *item;
  GtkTreeSelection *sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));

  if (gtk_tree_selection_count_selected_rows (sel) > 0)
    {
      GtkWidget *menu = gtk_menu_new ();

      /* Add */
      item = gtk_image_menu_item_new_from_stock (GTK_STOCK_ADD, NULL);
      gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), item);
      g_signal_connect (G_OBJECT (item), "activate",
			G_CALLBACK (shoutcast_add_selected), tree);

      /* Replace */
      item = gtk_image_menu_item_new_with_label (N_("Replace"));
      gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (item),
				     gtk_image_new_from_stock (GTK_STOCK_REDO,
							       GTK_ICON_SIZE_MENU));
      gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
      g_signal_connect (G_OBJECT (item), "activate",
			G_CALLBACK (shoutcast_replace_selected), tree);

      //      gmpc_mpddata_treeview_right_mouse_intergration(GMPC_MPDDATA_TREEVIEW(tree), GTK_MENU(menu));
      gtk_widget_show_all (menu);
      gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, event->button,
		      event->time);
      return TRUE;
    }

  return FALSE;
}

#if 0
static int
shoutcast_button_handle_release_event_tag_search_common (GtkWidget * button,
							 gpointer userdata,
							 gboolean subgenre)
{
  int position = GPOINTER_TO_INT (userdata);
  GtkTreeSelection *select =
    gtk_tree_view_get_selection (GTK_TREE_VIEW (treeviews[subgenre ? 1 : 0]));
  GtkTreeModel *model =
    gtk_tree_view_get_model (GTK_TREE_VIEW (treeviews[subgenre ? 1 : 0]));
  GtkTreeIter piter;
  struct genre_s *genrep = NULL;

  if (gtk_tree_selection_get_selected (select, &model, &piter))
    {
      gtk_tree_model_get (model, &piter, MPDDATA_MODEL_USERDATA, &genrep, -1);
    }

  if (position >= 1)
    {
      select =
	gtk_tree_view_get_selection (GTK_TREE_VIEW
				     (treeviews[subgenre ? 1 : 0]));
      model =
	gtk_tree_view_get_model (GTK_TREE_VIEW (treeviews[subgenre ? 1 : 0]));

      if (gtk_tree_selection_get_selected (select, &model, &piter))
	{
	  gtk_tree_model_get (model, &piter, MPDDATA_MODEL_USERDATA,
			      &genrep, -1);
	}
    }

  shoutcast_search (genrep);
  return 0;
}

static int
shoutcast_button_handle_release_event_tag_search (GtkWidget * button,
						  gpointer userdata)
{
  return shoutcast_button_handle_release_event_tag_search_common (button,
								  userdata,
								  FALSE);
}

static int
shoutcast_button_handle_release_event_tag_search_subgenre (GtkWidget * button,
							   gpointer userdata)
{
  return shoutcast_button_handle_release_event_tag_search_common (button,
								  userdata,
								  TRUE);
}

#endif

static int
shoutcast_button_handle_release_event_tag_refresh_common (GtkWidget * button,
							  gpointer userdata,
							  gboolean subgenre)
{
  GtkTreeSelection *select =
    gtk_tree_view_get_selection (GTK_TREE_VIEW (treeviews[subgenre ? 1 : 0]));
  GtkTreeModel *model =
    gtk_tree_view_get_model (GTK_TREE_VIEW (treeviews[subgenre ? 1 : 0]));
  GtkTreeIter piter;
  struct genre_s *genrep = NULL;
  gboolean search = FALSE;

  if (gtk_tree_selection_get_selected (select, &model, &piter))
    {
      gtk_tree_model_get (model, &piter, MPDDATA_MODEL_USERDATA, &genrep, -1);
    }

  select =
    gtk_tree_view_get_selection (GTK_TREE_VIEW (treeviews[subgenre ? 1 : 0]));
  model =
    gtk_tree_view_get_model (GTK_TREE_VIEW (treeviews[subgenre ? 1 : 0]));

  if (gtk_tree_selection_get_selected (select, &model, &piter))
    {
      gtk_tree_model_get (model, &piter, MPDDATA_MODEL_USERDATA, &genrep, -1);
    }

  refresh_genre (genrep, search);
  return 0;
}

static int
shoutcast_button_handle_release_event_tag_refresh (GtkWidget * button,
						   gpointer userdata)
{
  return shoutcast_button_handle_release_event_tag_refresh_common (button,
								   userdata,
								   FALSE);
}

static int
shoutcast_button_handle_release_event_tag_refresh_subgenre (GtkWidget *
							    button,
							    gpointer userdata)
{
  return shoutcast_button_handle_release_event_tag_refresh_common (button,
								   userdata,
								   TRUE);
}

static int
shoutcast_button_handle_release_event_tag_common (GtkWidget * tree,
						  GdkEventButton * event,
						  gpointer data,
						  gboolean subgenre)
{
  if (event->button == 1 && event->type == GDK_2BUTTON_PRESS)
    {
      shoutcast_button_handle_release_event_tag_refresh_common
	(GINT_TO_POINTER (0), data, subgenre);
      return TRUE;
    }

  if (event && event->button != 3)
    return FALSE;

  GtkWidget *item;
  GtkTreeSelection *sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));

  if (gtk_tree_selection_count_selected_rows (sel) > 0)
    {
      GtkWidget *menu = gtk_menu_new ();

      item = gtk_image_menu_item_new_from_stock (GTK_STOCK_REFRESH, NULL);
      gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), item);
      g_signal_connect (G_OBJECT (item), "activate",
			subgenre ?
			G_CALLBACK
			(shoutcast_button_handle_release_event_tag_refresh_subgenre)
			:
			G_CALLBACK
			(shoutcast_button_handle_release_event_tag_refresh),
			data);

      // FIXME (they do now): SHOUTcast doesn't actually use the genre for
      // searches. Its global.
#if 0
      item = gtk_image_menu_item_new_from_stock (GTK_STOCK_FIND, NULL);
      gtk_menu_shell_prepend (GTK_MENU_SHELL (menu), item);
      g_signal_connect (G_OBJECT (item), "activate",
			subgenre ?
			G_CALLBACK
			(shoutcast_button_handle_release_event_tag_search_subgenre)
			:
			G_CALLBACK
			(shoutcast_button_handle_release_event_tag_search),
			data);
#endif
      gtk_widget_show_all (menu);
      gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, event->button,
		      event->time);
      return TRUE;
    }

  return FALSE;
}

static int
shoutcast_button_handle_release_event_tag_subgenre (GtkWidget * tree,
						    GdkEventButton * event,
						    gpointer data)
{
  return shoutcast_button_handle_release_event_tag_common (tree, event, data,
							   TRUE);
}

static int
shoutcast_button_handle_release_event_tag (GtkWidget * tree,
					   GdkEventButton * event,
					   gpointer data)
{
  return shoutcast_button_handle_release_event_tag_common (tree, event, data,
							   FALSE);
}

static void
shoutcast_clear_search_entry (GtkEntry * entry,
			      GtkEntryIconPosition icon_pos,
			      GdkEvent * event, gpointer user_data)
{
  if (icon_pos == GTK_ENTRY_ICON_SECONDARY)
    {
      gtk_entry_set_text (GTK_ENTRY (shoutcast_search_entry), "");
    }
}

static int
shoutcast_search_entry_key_event (GtkWidget * w, GdkEventKey * event)
{
  if (event->keyval == GDK_KEY_Return)
    {
      g_free (search_text);
      search_text =
	g_strdup (gtk_entry_get_text (GTK_ENTRY (shoutcast_search_entry)));
      gtk_button_clicked (GTK_BUTTON (shoutcast_search_find));
      return TRUE;
    }

  return FALSE;
}

static void
genre_cursor_changed (GtkWidget * tree, gpointer data)
{
  GtkTreeSelection *s = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
  GtkTreeModel *model = gtk_tree_view_get_model (GTK_TREE_VIEW (tree));
  GtkTreeIter piter;

  if (gtk_tree_selection_get_selected (s, &model, &piter))
    {
      struct genre_s *genrep = NULL;

      s = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree));
      gtk_tree_model_get (model, &piter, MPDDATA_MODEL_USERDATA, &genrep, -1);

      if (!genrep)
	return;

      current_genre = genrep;
      shoutcast_show_song_list_common (s, data);
    }
}

static void
shoutcast_init ()
{
  int size;
  int needs_refresh = 0;
  GtkWidget *tree = NULL;
  GtkWidget *sw = NULL;
  GtkCellRenderer *renderer = NULL;
  GtkWidget *paned = NULL;
  GtkWidget *hbox = NULL;
  GtkWidget *vbox = NULL;
  GtkTreeModel *model = NULL;
  GtkTreeViewColumn *column = NULL;
  gchar *path = gmpc_plugin_get_data_path (&plugin);
  gchar *url =
    g_build_path (G_DIR_SEPARATOR_S, path, "shoutcastbrowser", NULL);
  config_obj *cfg;

  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  gtk_icon_theme_append_search_path (gtk_icon_theme_get_default (), url);
  g_free (url);
  g_free (path);

  station_column_width = 180;
  genre_column_width = 140;

  path = gmpc_get_user_path ("shoutcastbrowser.cfg");
  cfg = cfg_open (path);
  if (cfg)
    {
      station_column_width = cfg_get_single_value_as_int_with_default (cfg,
								       "shoutcastbrowser",
								       "station-column-width",
								       180);
      genre_column_width =
	cfg_get_single_value_as_int_with_default (cfg, "shoutcastbrowser",
						  "genre-column-width", 140);
    }

  g_free (path);
  shoutcastbrowser_cachedir = gmpc_get_cache_directory ("shoutcastbrowser");
  g_mkdir (shoutcastbrowser_cachedir, 0700);
  shoutcastbrowser_init (&needs_refresh);
  //gtk_init_add(shoutcast_new, NULL);
  shoutcast_vbox = gtk_hpaned_new ();
  gmpc_paned_size_group_add_paned (GMPC_PANED_SIZE_GROUP (paned_size_group),
				   GTK_PANED (shoutcast_vbox));

  vbox = gtk_vbox_new (FALSE, 6);
  /**
   * Create list store
   * 1. pointer 
   * 2. name
   */
  sb_store = gmpc_mpddata_model_new ();

  /* Paned window */
  paned = gtk_vbox_new (TRUE, 6);
  model = (GtkTreeModel *) gmpc_mpddata_model_new ();
  /* Genre list */
  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
				       GTK_SHADOW_ETCHED_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  treeviews[0] = gtk_tree_view_new_with_model (model);
  gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeviews[0]), TRUE);

  gtk_tree_view_set_search_column (GTK_TREE_VIEW (treeviews[0]),
				   MPDDATA_MODEL_COL_SONG_TITLE);
  g_signal_connect (G_OBJECT (treeviews[0]), "button-press-event",
		    G_CALLBACK (shoutcast_button_handle_release_event_tag),
		    GINT_TO_POINTER (0));
  g_signal_connect (G_OBJECT (treeviews[0]), "cursor-changed",
		    G_CALLBACK (genre_cursor_changed), NULL);
  /* Add column */
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, N_("Genre"));


  renderer = gtk_cell_renderer_pixbuf_new ();
  gtk_tree_view_column_pack_start (column, renderer, FALSE);
  gtk_tree_view_column_add_attribute (column, renderer, "icon-name",
				      MPDDATA_MODEL_COL_ICON_ID);
  gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_add_attribute (column, renderer, "text",
				      MPDDATA_MODEL_COL_SONG_TITLE);
  gtk_tree_view_insert_column (GTK_TREE_VIEW (treeviews[0]), column, -1);

  gtk_container_add (GTK_CONTAINER (sw), treeviews[0]);
  gtk_box_pack_start (GTK_BOX (paned), sw, TRUE, TRUE, 0);

  /* Sub-genre list */
  model = (GtkTreeModel *) gmpc_mpddata_model_new ();
  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
				       GTK_SHADOW_ETCHED_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  treeviews[1] = gtk_tree_view_new_with_model (model);
  gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (treeviews[1]), TRUE);
  gmpc_mpd_data_treeview_tooltip_new (GTK_TREE_VIEW (treeviews[1]),
				      META_ARTIST_ART);
  gtk_tree_view_set_search_column (GTK_TREE_VIEW (treeviews[1]),
				   MPDDATA_MODEL_COL_SONG_TITLE);
  g_signal_connect (G_OBJECT (treeviews[1]), "button-press-event",
		    G_CALLBACK
		    (shoutcast_button_handle_release_event_tag_subgenre),
		    GINT_TO_POINTER (0));
  g_signal_connect (G_OBJECT (treeviews[1]), "cursor-changed",
		    G_CALLBACK (genre_cursor_changed), NULL);
  /* Add column */
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, N_("Sub genre"));
  size =
    cfg_get_single_value_as_int_with_default (config, "gmpc-mpddata-model",
					      "icon-size", 64);
  gtk_tree_view_column_set_sizing (column, GTK_TREE_VIEW_COLUMN_FIXED);
  gtk_tree_view_set_fixed_height_mode (GTK_TREE_VIEW (treeviews[1]), TRUE);


  renderer = gtk_cell_renderer_pixbuf_new ();
  gtk_cell_renderer_set_fixed_size (renderer, size, size);

  gtk_tree_view_column_pack_start (column, renderer, FALSE);
  gtk_tree_view_column_add_attribute (column, renderer, "pixbuf",
				      MPDDATA_MODEL_META_DATA);
  renderer = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, renderer, TRUE);
  gtk_tree_view_column_add_attribute (column, renderer, "text",
				      MPDDATA_MODEL_COL_SONG_TITLE);
  gtk_tree_view_insert_column (GTK_TREE_VIEW (treeviews[1]), column, -1);

  gtk_container_add (GTK_CONTAINER (sw), treeviews[1]);
  gtk_box_pack_start (GTK_BOX (paned), sw, TRUE, TRUE, 0);


  g_signal_connect (G_OBJECT
		    (gtk_tree_view_get_selection
		     (GTK_TREE_VIEW (treeviews[0]))), "changed",
		    G_CALLBACK (shoutcast_show_genre_list), NULL);
  g_signal_connect (G_OBJECT
		    (gtk_tree_view_get_selection
		     (GTK_TREE_VIEW (treeviews[1]))), "changed",
		    G_CALLBACK (shoutcast_show_song_list), NULL);

  gtk_paned_add1 (GTK_PANED (shoutcast_vbox), paned);
  gtk_widget_show_all (paned);
  /**
   * tree
   */
  sw = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw),
				       GTK_SHADOW_ETCHED_IN);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
				  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);


  shoutcast_tree = tree =
    (GtkWidget *) gmpc_data_view_new ("shoutcastbrowser",
				      GMPC_DATA_VIEW_VIEW_TYPE_PLAYLIST);
  gmpc_mpddata_model_disable_image (GMPC_MPDDATA_MODEL (sb_store));
  gtk_tree_view_set_model (GTK_TREE_VIEW (tree), GTK_TREE_MODEL (sb_store));
  gtk_tree_view_set_search_column (GTK_TREE_VIEW (tree),
				   MPDDATA_MODEL_COL_SONG_TITLE);
  gtk_tree_view_set_enable_search (GTK_TREE_VIEW (tree), TRUE);
#if 0
  g_signal_connect (G_OBJECT (tree), "row-activated",
		    G_CALLBACK (magnatune_add_album_row_activated), NULL);

  g_signal_connect (G_OBJECT (tree), "key-press-event",
		    G_CALLBACK (magnatune_key_press), NULL);
#endif
  g_signal_connect (G_OBJECT (tree), "button-press-event",
		    G_CALLBACK (shoutcast_button_release_event), tree);

  gtk_container_add (GTK_CONTAINER (sw), tree);
  gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 0);

  gtk_widget_show_all (sw);
  gtk_widget_show (vbox);

  /**
   * Progress Bar for the bottom 
   */
  hbox = gtk_hbox_new (FALSE, 6);
  shoutcast_cancel = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
  g_signal_connect (G_OBJECT (shoutcast_cancel), "clicked",
		    G_CALLBACK (shoutcast_download_cancel), NULL);
  shoutcast_pb = gtk_progress_bar_new ();
  gtk_box_pack_start (GTK_BOX (hbox), shoutcast_pb, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), shoutcast_cancel, FALSE, TRUE, 0);
  gtk_box_pack_end (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);
  gtk_paned_add2 (GTK_PANED (shoutcast_vbox), vbox);

  /**
   * Search Bar for the bottom
   */
  hbox = gtk_hbox_new (FALSE, 6);
  shoutcast_search_find = gtk_button_new_from_stock (GTK_STOCK_FIND);
  g_signal_connect (G_OBJECT (shoutcast_search_find), "clicked",
		    G_CALLBACK (shoutcast_do_search), NULL);
  shoutcast_search_entry = gtk_entry_new ();
  path = cfg_get_single_value_as_string_with_default (cfg,
						      "shoutcastbrowser",
						      "search-text", "");
  gtk_entry_set_text (GTK_ENTRY (shoutcast_search_entry), path);
  g_free (path);
  gtk_entry_set_icon_from_stock (GTK_ENTRY (shoutcast_search_entry),
				 GTK_ENTRY_ICON_SECONDARY, GTK_STOCK_CLEAR);
  g_signal_connect (GTK_ENTRY (shoutcast_search_entry), "icon-press",
		    G_CALLBACK (shoutcast_clear_search_entry), NULL);
  g_signal_connect (GTK_ENTRY (shoutcast_search_entry), "key-press-event",
		    G_CALLBACK (shoutcast_search_entry_key_event), NULL);

  gtk_entry_set_activates_default (GTK_ENTRY (shoutcast_search_entry), TRUE);
  gtk_box_pack_start (GTK_BOX (hbox), shoutcast_search_entry, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), shoutcast_search_find, FALSE, TRUE, 0);
  gtk_box_pack_end (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);
  gtk_paned_add2 (GTK_PANED (shoutcast_vbox), vbox);

  column = gtk_tree_view_get_column (GTK_TREE_VIEW (shoutcast_tree), 0);
  gtk_tree_view_column_set_visible (column, FALSE);
  column = gtk_tree_view_get_column (GTK_TREE_VIEW (shoutcast_tree), 1);
  gtk_tree_view_column_set_sort_column_id (column,
					   MPDDATA_MODEL_COL_SONG_TITLE);
  gtk_tree_view_column_set_title (column, N_("Station"));

  column = gtk_tree_view_get_column (GTK_TREE_VIEW (shoutcast_tree), 7);
  gtk_tree_view_column_set_sort_column_id (column,
					   MPDDATA_MODEL_COL_SONG_NAME);
  gtk_tree_view_column_set_title (column, N_("Playing"));

  column = gtk_tree_view_get_column (GTK_TREE_VIEW (shoutcast_tree), 5);
  gtk_tree_view_column_set_sort_column_id (column,
					   MPDDATA_MODEL_COL_SONG_GENRE);
  gtk_tree_view_column_set_sort_indicator (column, TRUE);

  column = gtk_tree_view_get_column (GTK_TREE_VIEW (shoutcast_tree), 13);
  gtk_tree_view_column_set_title (column, N_("Type"));
  gtk_tree_view_column_set_sort_column_id (column,
					   MPDDATA_MODEL_COL_SONG_COMMENT);
  gtk_tree_view_column_set_sort_indicator (column, TRUE);

  /* These columns are integers and are not sortable in the usual way. */
  column = gtk_tree_view_get_column (GTK_TREE_VIEW (shoutcast_tree), 10);
  gtk_tree_view_column_set_title (column, N_("Bitrate"));
#if 0
  gtk_tree_view_column_set_sort_column_id (column,
					   MPDDATA_MODEL_COL_SONG_DATE);
  gtk_tree_view_column_set_sort_indicator (column, TRUE);
#endif
  column = gtk_tree_view_get_column (GTK_TREE_VIEW (shoutcast_tree), 12);
  gtk_tree_view_column_set_title (column, N_("Listeners"));
#if 0
  gtk_tree_view_column_set_sort_column_id (column,
					   MPDDATA_MODEL_COL_SONG_DISC);
  gtk_tree_view_column_set_sort_indicator (column, TRUE);
#endif

  g_object_ref (shoutcast_vbox);
  if (needs_refresh)
    get_genre_list ();
  else
    show_genre_list ();

  cfg_close (cfg);
}

static void
shoutcast_selected (GtkWidget * container)
{
  if (shoutcast_vbox == NULL)
    {
      shoutcast_init ();
      gtk_container_add (GTK_CONTAINER (container), shoutcast_vbox);
      gtk_widget_show (shoutcast_vbox);
      get_genre_list ();
    }
  else
    {
      gtk_container_add (GTK_CONTAINER (container), shoutcast_vbox);
      gtk_widget_show (shoutcast_vbox);
    }
}

static void
shoutcast_unselected (GtkWidget * container)
{
  gtk_container_remove (GTK_CONTAINER (container), shoutcast_vbox);
}

#if 0
static int
shoutcast_button_handle_release_event_tag_add (GtkWidget * button,
					       gpointer userdata)
{
  GList *list;
  MpdData *data;
  int position = GPOINTER_TO_INT (userdata);
  gchar *genre = NULL, *artist = NULL, *album = NULL;
  GtkTreeSelection *select =
    gtk_tree_view_get_selection (GTK_TREE_VIEW (treeviews[0]));
  GtkTreeModel *model =
    gtk_tree_view_get_model (GTK_TREE_VIEW (treeviews[0]));
  GtkTreeIter piter;
  if (gtk_tree_selection_get_selected (select, &model, &piter))
    {
      gtk_tree_model_get (model, &piter, MPDDATA_MODEL_COL_SONG_TITLE, &genre,
			  -1);
    }
  if (position >= 1)
    {
      select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeviews[1]));
      model = gtk_tree_view_get_model (GTK_TREE_VIEW (treeviews[1]));
      if (gtk_tree_selection_get_selected (select, &model, &piter))
	{
	  gtk_tree_model_get (model, &piter, MPDDATA_MODEL_COL_SONG_TITLE,
			      &artist, -1);
	}
    }
  if (position >= 2)
    {
      select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeviews[2]));
      model = gtk_tree_view_get_model (GTK_TREE_VIEW (treeviews[2]));
      if (gtk_tree_selection_get_selected (select, &model, &piter))
	{
	  gtk_tree_model_get (model, &piter, MPDDATA_MODEL_COL_SONG_TITLE,
			      &album, -1);
	}
    }

  data = magnatune_db_get_song_list (genre, artist, album, TRUE);
  for (data = mpd_data_get_first (data); data;
       data = mpd_data_get_next (data))
    {
      mpd_playlist_queue_add (connection, data->song->file);
    }
  mpd_playlist_queue_commit (connection);
  if (genre)
    g_free (genre);
  if (artist)
    g_free (artist);
  if (album)
    g_free (album);

}

static int
magnatune_button_handle_release_event_tag_replace (GtkWidget * button,
						   gpointer userdata)
{
  mpd_playlist_clear (connection);
  magnatune_button_handle_release_event_tag_add (button, userdata);
  mpd_player_play (connection);

}
#endif

#if 0
static int
magnatune_key_press (GtkWidget * tree, GdkEventKey * event)
{
  if (event->state & GDK_CONTROL_MASK && event->keyval == GDK_Insert)
    {
      magnatune_replace_selected (NULL, GTK_TREE_VIEW (tree));
    }
  else if (event->keyval == GDK_Insert)
    {
      magnatune_add_selected (NULL, GTK_TREE_VIEW (tree));
    }
  return FALSE;
}
#endif

static void
shoutcast_save_myself (void)
{
  if (shoutcastbrowser_ref)
    {
      GtkTreePath *path =
	gtk_tree_row_reference_get_path (shoutcastbrowser_ref);

      if (path)
	{
	  gint *indices = gtk_tree_path_get_indices (path);

	  debug_printf (DEBUG_INFO, "Saving myself to position: %i\n",
			indices[0]);
	  cfg_set_single_value_as_int (config, "shoutcastbrowser", "position",
				       indices[0]);
	  gtk_tree_path_free (path);
	}
    }
}

static void
shoutcast_destroy (void)
{
  gchar *path = gmpc_get_user_path ("shoutcastbrowser.cfg");
  config_obj *cfg = cfg_open (path);

  if (cfg)
    {
      cfg_set_single_value_as_string (cfg, "shoutcastbrowser", "search-text",
				      search_text);
    }

  g_free (path);
  cfg_close (cfg);
}

static int
shoutcast_cat_menu_popup (GtkWidget * menu, int type,
			  GtkWidget * tree, GdkEventButton * event)
{
  GtkWidget *item;

  if (type != plugin.id)
    return 0;
  else if (!downloading)
    {
      /* add the clear widget */
      item = gtk_image_menu_item_new_from_stock (GTK_STOCK_REFRESH, NULL);
      gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
      g_signal_connect (G_OBJECT (item), "activate",
			G_CALLBACK (get_genre_list), NULL);
      return 1;
    }

  return 0;
}

static const gchar *
get_translation_domain (void)
{
  return GETTEXT_PACKAGE;
}

gmpcPlBrowserPlugin shoutcast_gbp = {
  .add = shoutcast_add,
  .selected = shoutcast_selected,
  .unselected = shoutcast_unselected,
  .cat_right_mouse_menu = shoutcast_cat_menu_popup,
  .integrate_search = NULL,
  .integrate_search_field_supported = NULL,
};

gmpcPrefPlugin shoutcast_gpp = {
  shoutcast_pref_construct,
  shoutcast_pref_destroy
};

gmpcPlugin plugin = {
  .name = N_("SHOUTcast Browser"),
  .version =
    {PLUGIN_MAJOR_VERSION, PLUGIN_MINOR_VERSION, PLUGIN_MICRO_VERSION},
  .plugin_type = GMPC_PLUGIN_PL_BROWSER,
  .init = shoutcast_init,
  .destroy = shoutcast_destroy,
  .save_yourself = shoutcast_save_myself,
  .mpd_status_changed = shoutcast_mpd_status_changed,
  .browser = &shoutcast_gbp,
  .pref = &shoutcast_gpp,
  .get_enabled = shoutcast_get_enabled,
  .set_enabled = shoutcast_set_enabled,
  .get_translation_domain = get_translation_domain,
};
