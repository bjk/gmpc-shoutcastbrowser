/* gmpc-shoutcastbrowser (GMPC plugin)
 * Copyright (C) 2014 Ben Kibbey <bjk@luxsci.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/
#ifndef SHOUTCAST_H
#define SHOUTCAST_H

struct station_s
{
  char *name;
  char *id;
  char *genre;
  int listeners;
  int bitrate;
  char *type;
  char *track;
};

struct genre_s
{
  char *name;
  struct station_s **stations;
  int s_total;
  struct genre_s **sub;
  size_t g_total;
};

extern gchar *shoutcastbrowser_cachedir;

int shoutcastbrowser_init (int *refresh);
void shoutcastbrowser_deinit ();
MpdData *shoutcastbrowser_genre_list ();
MpdData *shoutcastbrowser_subgenre_list (struct genre_s *);
MpdData *shoutcastbrowser_station_list (struct genre_s *);
void shoutcastbrowser_data_ready (const char *data, size_t len,
				  struct genre_s *);
struct genre_s *shoutcastbrowser_genre_by_index (int index);

#endif
